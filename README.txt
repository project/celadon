$Id

** INSTALLATION **
1. Copy Fusion (http://drupal.org/project/fusion) to your themes folder, typically sites/all/themes.
2. Copy Celadon to your themes folder, typically sites/all/themes.
3. Enable Fusion (required to utilize Skinr classes).
4. Enable Celadon and select it as the default theme.


** NOTES **
1. Custom CSS should be added to local.css (rename local-sample.css in the CSS folder to activate).
2. Grant desired permissions for the search module.
3. Sidebar blocks should be added to the "Sidebar First" region
